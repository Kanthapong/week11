package com.pete.week11;

public interface Walkable {
    public void walk();
    public void run();
}
