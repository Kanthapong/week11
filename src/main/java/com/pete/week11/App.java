package com.pete.week11;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Bird bird = new Bird("Tweety");
        bird.eat();
        bird.sleep();
        bird.takeoff();
        bird.fly();
        bird.landing();
        bird.walk();
        bird.run();
        Plane boeing = new Plane("Boeing", "Rolls royce");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Superman clark = new Superman("Clark");
        clark.eat();
        clark.sleep();
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk();
        clark.run();
        clark.swim();
        Cat tom = new Cat("Tom");
        tom.eat();
        tom.sleep();
        tom.walk();
        tom.run();
        Rat jerry = new Rat("Jerry");
        jerry.eat();
        jerry.sleep();
        jerry.walk();
        jerry.run();
        Dog tomas = new Dog("Tomas");
        tomas.eat();
        tomas.sleep();
        tomas.walk();
        tomas.run();
        Bat kiki = new Bat("Kiki");
        kiki.takeoff();
        kiki.fly();
        kiki.landing();
        kiki.eat();
        kiki.sleep();
        Fish nemo = new Fish("Nemo");
        nemo.swim();
        nemo.eat();
        nemo.sleep();
        Crocodile coco = new Crocodile("Coco");
        coco.craw();
        coco.swim();
        coco.walk();
        coco.run();
        coco.eat();
        coco.sleep();
        Snake lulu = new Snake("Lulu");
        lulu.craw();
        lulu.swim();
        lulu.eat();
        lulu.sleep();
        Human man = new Human("Man");
        man.eat();
        man.sleep();
        man.walk();
        man.run();
        man.swim();

        Flyable[] flyables = {bird, boeing, clark, kiki};
        for(int i=0; i<flyables.length; i++){
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = {bird,  clark, man, tom, tomas, jerry, coco};
        for(int i=0; i<walkables.length; i++){
            walkables[i].walk();
            walkables[i].run();
        }

        Swimable[] swimables = {clark, man, coco, lulu,nemo};
        for(int i=0; i<swimables.length; i++){
            swimables[i].swim();
        }

        Crawable[] crawables = {coco, lulu};
        for(int i=0; i<crawables.length; i++){
            crawables[i].craw();
        }
    }
}
