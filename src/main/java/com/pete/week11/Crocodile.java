package com.pete.week11;

public class Crocodile extends Animal implements Crawable,Walkable,Swimable{

    public Crocodile(String name) {
        super(name, 4);
    }

    @Override
    public void swim() {
        System.out.println(this+ " swim.");
    }

    @Override
    public void walk() {
        System.out.println(this+ " walk.");
    }

    @Override
    public void run() {
        System.out.println(this+ " run.");
    }

    @Override
    public void craw() {
        System.out.println(this+ " craw.");
    }

    @Override
    public void sleep() {
        System.out.println(this+ " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this+ " eat.");
    }
    
    @Override
    public String toString() {
        return "Crocodile ("+this.getName()+")";
    }
}
