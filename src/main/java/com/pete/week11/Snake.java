package com.pete.week11;

public class Snake extends Animal implements Crawable,Swimable{

    public Snake(String name) {
        super(name, 0);
    }

    @Override
    public void swim() {
        System.out.println(this+ " swim.");
    }

    @Override
    public void craw() {
        System.out.println(this+ " craw.");
    }

    @Override
    public void sleep() {
        System.out.println(this+ " sleep.");
    }

    @Override
    public void eat() {
        System.out.println(this+ " eat.");
    }

    @Override
    public String toString() {
        return "Snake ("+this.getName()+")";
    }
    
}
